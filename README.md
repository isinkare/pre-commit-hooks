Add these hooks by creating `.pre-commit-config.yaml` in your project's root directory, and adding the following YAML:

```yml
repos:
  - repo: https://gitlab.cern.ch/isinkare/pre-commit-hooks
    rev: 'v0.1.0'
    hooks:
      - id: mypy
        additional_dependencies:
          # Any additional libraries that are needed for type checking to passm e.g.
          - "types-mock"
```

Then, exclude `.mypy-pre-commit` directory through your `.gitignore` definition.
