#!/usr/bin/env sh
set -eu

VENV_DIR="./.mypy-pre-commit"

if [ ! -d "$VENV_DIR" ]; then
  python -m venv "$VENV_DIR"
  # Newer pip is needed to access --config-settings option
  "$VENV_DIR/bin/python" -m pip install 'pip>=23.1'
  # strict mode is needed to overcome type checkers not handling editable mode
  # (https://github.com/pypa/setuptools/issues/3518)
  "$VENV_DIR/bin/python" -m pip install \
      --quiet \
      --config-settings editable_mode=strict \
      --verbose \
      --editable \
      .[dev]
fi
"$VENV_DIR/bin/python" -m mypy \
  "${@}"
